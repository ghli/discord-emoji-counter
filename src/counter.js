const runes = require("runes");

const cleanCompact = (s) =>
  s
    .split("\n")
    .map((x) => x.replace(/\[?.*?\].*?:/, "").replace(/\s+/g, ""))
    .join("");

const cleanCozy = (s) =>
s
    .split("\n")
    .filter((x,i) => i % 2 === 1)
    .map(x => x.replace(/\s+/g, ""))
    .join("");

const symbolize = (s, m = new Map()) => ({
  s: [...s]
    .reduce(
      (s, x) => {
        if (!/[a-zA-Z:]/.exec(x)) {
          s.a.push(x);
          return s;
        }
        s.cs += x;
        if (/:.*:/.exec(s.cs)) {
          if (s.m.get(s.cs) == undefined) {
            s.m.set(s.cs, s.n);
            s.a.push(s.n);
            s.n++, (s.cs = "");
          } else {
            s.m.set(s.cs, s.m.get(s.cs));
            s.a.push(s.m.get(s.cs));
            s.cs = "";
          }
        }
        return s;
      },
      { cs: "", n: m.size, m, a: [] }
    )
    .a.join(""),
  m: new Map([...m.entries()].map(([a, b]) => [`${b}`, a])),
});

const count = ({ s: a, m: mm }) =>
  runes(a).reduce(
    ({ m, mm }, x) => {
      m.set(mm.get(x) || x, 1 + (m.get(mm.get(x) || x) || 0));
      return { m, mm };
    },
    { m: new Map(), mm }
  ).m;

const format = (m) =>
  [...m.entries()]
    .sort((a, b) => b[1] - a[1])
    .map(([x, n]) => `${x}: ${n}`)
    .join("\n");

const formattedCount = (str, mode = 'cozy') => format(count(symbolize((mode === 'compact' ? cleanCompact : mode == 'cozy' ? cleanCozy : (x => x))(str))));

module.exports = {
  cleanCozy,
  cleanCompact,
  symbolize,
  count,
  format,
  formattedCount,
};
