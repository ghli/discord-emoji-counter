const {formattedCount, cleanCozy} = require('../src/counter');
function submit() {
    const textarea = document.getElementById('fname');
    const output = document.getElementById('output');
    const radios = document.querySelectorAll('input[type="radio"][name="displayMode"]');
    const checkedRadioValue = [...radios].find(x => x.checked).value;
    output.innerText = '';
    setTimeout(() => {
        output.innerText = formattedCount(textarea.value, checkedRadioValue);
    }, 500);
};
document.getElementById("form").onsubmit = submit;
